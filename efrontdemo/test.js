<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html    >
<head>
    <base href = "http://localhost:8080/efront/www/">    
    <meta http-equiv = "Content-Language" content = "en">
    <meta http-equiv = "keywords"         content = "education">
    <meta http-equiv = "description"      content = "Collaborative Elearning Platform">
    <meta http-equiv = "Content-Type"     content = "text/html; charset = utf-8">
    <!--  <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>  -->
    <link rel="shortcut icon" href="themes/default/images/favicon.png">
    <link rel = "stylesheet" type = "text/css" href = "teststyle">
        <title>eFront | Refreshing eLearning</title>
<!-- 
    -->
    <link rel = "stylesheet" type = "text/css" href = "js/bootstrap/css/bootstrap.css" />
    <link rel = "stylesheet" type = "text/css" href = "js/includes/imgareaselect/css/imgareaselect-default.css" />
    <script src = "js/bootstrap/jquery.js"></script>
    <script src = "js/bootstrap/js/bootstrap.js"></script>
    <script src = "js/scripts.js"></script>
    <script src = "js/includes/imgareaselect/scripts/jquery.imgareaselect.pack.js"></script>
    <script type = "text/javascript">
        var ajaxObjects    = new Array();
        						
        top.document.title = "eFront | Refreshing eLearning";
        if (window.name == 'POPUP_FRAME') var popup=1;
        var globalImageExtension = 'png';
                var usingHorizontalInterface = true;
        
        var sessionLogin = "admin";
        var translationsToJS = new Array();
    </script>


<script>var translations = new Array();	/*used for passing language tags to js*/</script>

</head>
<body id = "body_control_panel" onkeypress = "if (window.eF_js_keypress) eF_js_keypress(event);" onbeforeunload = "if (window.periodicUpdater) periodicUpdater(false);">




<script language = "JavaScript" type = "text/javascript">



 </script>


	
	    
		
		        	
        	        
	    

   		
        
        
        
        
        
        
	    
                	
                    

















	










		<script>
	    // Translations used in the updater script
	    
	    translations['lessons']         = 'Lessons';
	    translations['servername']      = 'http://localhost:8080/efront/www/';
	    translations['onlineusers']     = 'Connected';
	    translations['nousersinroom']   = 'No user currently in this chat room';
	    translations['redirectedtomain']= 'Redirected to general room';
	    translations['s_type']          = 'administrator';
	    translations['s_login']         = 'admin';
	    translations['clicktochange']   = 'Click to change status';
	    translations['userisonline'] 	= 'The user is online';
	    translations['and'] 			= 'and';
	    translations['hours'] 			= 'hours';
	    translations['minutes'] 		= 'minutes';
	    translations['userjustloggedin']= 'The user just logged in';
	    translations['user'] 			= 'User';
	    translations['sendmessage'] 	= 'Send message';
	    translations['web'] 			= 'Personal page';
		translations['user_stats']  	= 'User reports';
		translations['user_settings']	= 'Public profile';
		translations['logout_user']		= 'Log out user';
		translations['_ADMINISTRATOR']	= 'Administrator';
		translations['_PROFESSOR']		= 'Professor';
		translations['_STUDENT']		= 'Student';
		translations['_IRREVERSIBLEACTIONAREYOUSURE'] = 'This operation is irreversible! Are you sure?';
	
		var startUpdater = true;
		var updaterPeriod = '100000';
	</script>
		<table class = "pageLayout centerFull" id = "pageLayout">
		<tr><td style = "vertical-align:top">
			<table style = "width:100%;">
							<tr><td id ="horizontalBarRow" class = "header" colspan = "3">	<div id = "logo">
		<a href = "administrator.php">
			<img  class = 'handle' src = "themes/efront2013/images/logo/logo.png?" title = "eFront" alt = "eFront" />
		</a>
	</div>
	
		<div id = "logout_link" >
					  					            	<span class="headerText dropdown">
		            	<span class = "label" style = "cursor:pointer" title = "Switch to simple mode" onclick = "jQuery.fn.efront('switchmode')">Complete mode</span>
	            	</span>
	            	            	            <span class="headerText dropdown">
	                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Go to <b class="caret"></b></a>
	                <ul class="dropdown-menu">
	                	<li><a href="userpage.php">Home</a></li>
	                		                	<li><a href="userpage.php?ctg=users">Users</a></li>
	                		                		                	<li><a href="userpage.php?ctg=courses">Courses</a></li>
	                	<li><a href="userpage.php?ctg=lessons">Lessons</a></li>
							    
					 	                	<li class="divider"></li>
						<li class="nav-header">Add</li>
							                	
	                	<li><a href="userpage.php?ctg=personal&user=admin&op=profile&add_user=1">User</a></li>
	                		                		                	<li><a href="userpage.php?ctg=courses&add_course=1">Course</a></li>
	                	<li><a href="userpage.php?ctg=lessons&add_lesson=1">Lesson</a></li>
							    
					 	                </ul>
	            </span>
	            	            <span class="headerText dropdown">
	                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">Administrator S. (admin) <b class="caret"></b></a>
	                <ul class="dropdown-menu">
						                    <li class="nav-header">Switch account</li>
																			<li><a href="javascript:void(0)" onclick = "changeAccount('professor')">Professor D. (professor)</a></li>
			                						<li><a href="javascript:void(0)" onclick = "changeAccount('student')">Student D. (student)</a></li>
			                			            	                    <li class="divider"></li>
		            	            		                  	<li><a href="userpage.php?ctg=personal&user=admin&op=dashboard">Dashboard</a></li>
	              		                  	<li><a href="userpage.php?ctg=personal&user=admin&op=profile">Account</a></li>
	              		              	 	                </ul>
	            </span>				
				            <span class="headerText dropdown">
	                <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Messages <b class="caret"></b></a> 	                	                <ul class="dropdown-menu">
	                	<li><a href="userpage.php?ctg=messages">Incoming</a></li>
	                	<li><a href="userpage.php?ctg=messages&add=1">Create</a></li>
	                </ul>
	                	            </span>
										<span class = "headerText">
	            <form action = "/efront/www/administrator.php?ctg=control_panel&op=search" method = "post" style = "display:inline-block;">
					<input type = "text" name = "search_text" placeholder = "Search" class = "searchBox"/>
					<input type = "hidden" name = "current_location" id = "current_location" />
				</form>
				</span>
						  				
			 				<a class = "headerText" href = "index.php?logout=true">Logout</a>
								</div>
			<div id = "path">
		<div id = "path_title"><a class = "titleLink" title="Home" href ="/efront/www/administrator.php?ctg=control_panel">Home</a></div>
		<div id = "tab_handles_div">
					</div>
	</div>
</td></tr>
						<tr><td class = "layoutColumn left">
									</td>
				<td class = "layoutColumn center">
									<table class = "centerTable">
		
				<tr>
	    <td class = "moduleCell">
	        <div id="sortableList">
	            <div style="float: right; width:49.5%;height: 100%;">
	                <ul class="sortable" id="secondlist" style="width:100%;">
								                    <li id="secondlist_moduleNewsList">
	                        <table class = "singleColumnData">
	                            			<tr><td class = "moduleCell">
		        
		        
    <div class = "block" style = ";" id = "%3Ca+href+%3D+%22administrator.php%3Fctg%3Dnews%22%3EAnnouncements%3C%2Fa%3E" >
        <div class = "blockContents" >
    			<span class = "handles"><a  href = "administrator.php?ctg=news&add=1&popup=1" onclick = "eF_js_showDivPopup(event, 'Add announcement', 2)" target = "POPUP_FRAME" ><img src = 'themes/default/images/others/transparent.gif' class = 'sprite16 sprite16-add' title = 'Add announcement' alt = 'Add announcement' /></a><a  href = "administrator.php?ctg=news"   ><img src = 'themes/default/images/others/transparent.gif' class = 'sprite16 sprite16-go_into' title = 'Go to announcements page' alt = 'Go to announcements page' /></a><img  src = 'themes/default/images/others/transparent.gif'  class = 'open open-close-handle sprite16 sprite16-navigate_up' alt = "Expand/collapse block" title = "Expand/collapse block" onclick = "toggleBlock(this, '0be4bb42fc5e9fe4f28e56d69dddd609')"  id = "%3Ca+href+%3D+%22administrator.php%3Fctg%3Dnews%22%3EAnnouncements%3C%2Fa%3E_image"><img  src = 'themes/default/images/others/transparent.gif'  class = 'blockMoveHandle sprite16 sprite16-attachment' alt = "Move block" title = "Move block" onmousedown = "createSortable('firstlist');createSortable('secondlist');if (window.showBorders) showBorders(event)" onmouseup = "if (window.showBorders) hideBorders(event)"></span>
        		<span class = "title"><a href = "administrator.php?ctg=news">Announcements</a></span>
        		<span class = "subtitle"></span>
        		
        		<div class = "content" style = ";" id = "%3Ca+href+%3D+%22administrator.php%3Fctg%3Dnews%22%3EAnnouncements%3C%2Fa%3E_content" onmousedown = "if ($('firstlist')) {Sortable.destroy('firstlist');}if ($('secondlist')) {Sortable.destroy('secondlist');}">
							        	<table class = "cpanelTable">
		        			        		<tr><td class = "emptyCategory">No announcements</td></tr>
		        			        	</table>
		        
				</div>
        		<span style = "display:none">&nbsp;</span>
        </div>
    </div>
			</td></tr>
        
	                        </table>
	                    </li>
			                    <li id="secondlist_moduleCalendar">
	                        <table class = "singleColumnData">
	                                        <tr><td class = "moduleCell">
                                                
    <div class = "block" style = ";" id = "%3Ca+href+%3D+%22administrator.php%3Fctg%3Dcalendar%22%3ECalendar+%28%23filter%3Atimestamp-1490562000%23%29%3C%2Fa%3E" >
        <div class = "blockContents" >
    			<span class = "handles"><a  href = "administrator.php?ctg=calendar&add=1&view_calendar=1490562000&popup=1" onclick = "eF_js_showDivPopup(event, 'Add calendar event', 3)" target = "POPUP_FRAME" ><img src = 'themes/default/images/others/transparent.gif' class = 'sprite16 sprite16-add' title = 'Add calendar event' alt = 'Add calendar event' /></a><a  href = "administrator.php?ctg=calendar"   ><img src = 'themes/default/images/others/transparent.gif' class = 'sprite16 sprite16-go_into' title = 'Go to calendar' alt = 'Go to calendar' /></a><img  src = 'themes/default/images/others/transparent.gif'  class = 'open open-close-handle sprite16 sprite16-navigate_up' alt = "Expand/collapse block" title = "Expand/collapse block" onclick = "toggleBlock(this, '6f25f6d65fe04cb5aedc66eda37d0f85')"  id = "%3Ca+href+%3D+%22administrator.php%3Fctg%3Dcalendar%22%3ECalendar+%28%23filter%3Atimestamp-1490562000%23%29%3C%2Fa%3E_image"><img  src = 'themes/default/images/others/transparent.gif'  class = 'blockMoveHandle sprite16 sprite16-attachment' alt = "Move block" title = "Move block" onmousedown = "createSortable('firstlist');createSortable('secondlist');if (window.showBorders) showBorders(event)" onmouseup = "if (window.showBorders) hideBorders(event)"></span>
        		<span class = "title"><a href = "administrator.php?ctg=calendar">Calendar (27 Mar 2017)</a></span>
        		<span class = "subtitle"></span>
        		
        		<div class = "content" style = ";" id = "%3Ca+href+%3D+%22administrator.php%3Fctg%3Dcalendar%22%3ECalendar+%28%23filter%3Atimestamp-1490562000%23%29%3C%2Fa%3E_content" onmousedown = "if ($('firstlist')) {Sortable.destroy('firstlist');}if ($('secondlist')) {Sortable.destroy('secondlist');}">
					                    
    <table>
        <tr><td>
            <table class = "calendarHeader" >
                <tr class = "calendar">
                    <td class = "calendarHeader">
                        <a href = "administrator.php?ctg=calendar&view_calendar=1485900000">&laquo; </a>
                        Mar
                        <a href = "administrator.php?ctg=calendar&view_calendar=1490994000">&raquo; </a>
                    </td>
                    <td class = "calendarHeader" style = "text-align:right">
                        <a href = "administrator.php?ctg=calendar&view_calendar=1456783200">&laquo; </a>
                        2017
                        <a href = "administrator.php?ctg=calendar&view_calendar=1519855200"> &raquo;</a>
                    </td></tr>
            </table>
        </td></tr>
        <tr><td>
            <table class = "calendar">
                <tr><td class = "calendar">&nbsp;M&nbsp;</td>
                    <td class = "calendar">&nbsp;T&nbsp;</td>
                    <td class = "calendar">&nbsp;W&nbsp;</td>
                    <td class = "calendar">&nbsp;T&nbsp;</td>
                    <td class = "calendar">&nbsp;F&nbsp;</td>
                    <td class = "calendar">&nbsp;S&nbsp;</td>
                    <td class = "calendar">&nbsp;S&nbsp;</td>
                </tr><tr>
            <tr>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488232800"></a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488232800"></a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488319200">1</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488405600">2</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488492000">3</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488578400">4</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488664800">5</a></td>
            </tr>
            <tr>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488751200">6</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488837600">7</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488924000">8</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489010400">9</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489096800">10</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489183200">11</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489269600">12</a></td>
            </tr>
            <tr>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489356000">13</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489442400">14</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489528800">15</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489615200">16</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489701600">17</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489788000">18</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489874400">19</a></td>
            </tr>
            <tr>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1489960800">20</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490047200">21</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490133600">22</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490220000">23</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490306400">24</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490392800">25</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490479200">26</a></td>
            </tr>
            <tr>
                <td class = "calendar todayCalendar viewCalendar">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490562000">27</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490648400">28</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490734800">29</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490821200">30</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1490907600">31</a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488232800"></a></td>
                <td class = "calendar ">
                    <a href = "administrator.php?ctg=calendar&view_calendar=1488232800"></a></td>
            </tr>
                </table>
        </td></tr>
    </table>
                
				</div>
        		<span style = "display:none">&nbsp;</span>
        </div>
    </div>
            </td></tr>
    	
	                        </table>
	                    </li>
					
			
	                                        <li id="secondlist_modulesecurity">
                            <table class = "singleColumnData">
                                                                <tr><td class = "moduleCell">
                                        
    
    <div class = "block" style = ";" id = "Security+module" >
        <div class = "blockContents" >
    			<span class = "handles"><a  href = "administrator.php?ctg=module&op=module_security"   ><img src = 'http://localhost:8080/efront/www/modules/module_security/img/go_into.png' title = 'Details' alt = 'Details' /></a><img  src = 'themes/default/images/others/transparent.gif'  class = 'open open-close-handle sprite16 sprite16-navigate_up' alt = "Expand/collapse block" title = "Expand/collapse block" onclick = "toggleBlock(this, 'f2c9e65d5c03d49f4a5e07935c8fe796')"  id = "Security+module_image"><img  src = 'themes/default/images/others/transparent.gif'  class = 'blockMoveHandle sprite16 sprite16-attachment' alt = "Move block" title = "Move block" onmousedown = "createSortable('firstlist');createSortable('secondlist');if (window.showBorders) showBorders(event)" onmouseup = "if (window.showBorders) hideBorders(event)"></span>
        		<span class = "title">Security module</span>
        		<span class = "subtitle"></span>
        		
        		<div class = "content" style = ";" id = "Security+module_content" onmousedown = "if ($('firstlist')) {Sortable.destroy('firstlist');}if ($('secondlist')) {Sortable.destroy('secondlist');}">
					        	<table style = "width:100%;">
        		<tr><td style = "vertical-align:top;">
                	<ul style = "padding-left:0px;margin-left:0px;list-style-type:none;">
													<li><a href = "administrator.php?ctg=module&op=module_security&type=changed_files" style = "color:red">Critical security issue: 9 system files have changed in the filesystem</a></li>
													<li><a href = "administrator.php?ctg=module&op=module_security&type=new_files" style = "color:red">Critical security issue: 22 new PHP files found in the filesystem</a></li>
						                	</ul>
                	<ul style = "padding-left:0px;margin-left:0px;list-style-type:none;">
						<li> 14 Nov 2014 <a href = "http://security.efrontlearning.net/2014/11/efront-version-3615-is-released.html" target = "_NEW">eFront version 3.6.15 is released</a></li><li> 05 Feb 2014 <a href = "http://security.efrontlearning.net/2014/02/efront-version-3614-is-released.html" target = "_NEW">eFront version 3.6.14 is released</a></li><li> 27 May 2013 <a href = "http://security.efrontlearning.net/2013/05/an-update-for-version-3613-is-available.html" target = "_NEW">An update for version 3.6.13 is available</a></li><li> 09 Apr 2013 <a href = "http://security.efrontlearning.net/2013/04/efront-version-3613-is-released.html" target = "_NEW">eFront version 3.6.13 is released</a></li><li> 24 Dec 2012 <a href = "http://security.efrontlearning.net/2012/12/efront-version-3612-is-released.html" target = "_NEW">eFront version 3.6.12 is released</a></li><li> 24 Apr 2012 <a href = "http://security.efrontlearning.net/2012/04/update-for-efront-3611-is-available.html" target = "_NEW">An update for eFront 3.6.11 is available</a></li><li> 10 Apr 2012 <a href = "http://security.efrontlearning.net/2012/04/new-version-efront-3611-build-14714.html" target = "_NEW">New version: eFront 3.6.11 build 14714</a></li><li> 28 Oct 2011 <a href = "http://security.efrontlearning.net/2011/10/patches-available-for-version-369-build.html" target = "_NEW">Patches available for version 3.6.9 build 11018 of eFront</a></li><li> 26 Oct 2011 <a href = "http://security.efrontlearning.net/2011/10/security-alert-efront-multiple.html" target = "_NEW">Security alert: eFront Multiple vulnerabilities</a></li><li> 26 Oct 2011 <a href = "http://security.efrontlearning.net/2011/10/module-security-is-released.html" target = "_NEW">Module "security" is released</a></li>
                	</ul>
        		</td></tr>
        	</table>
    
				</div>
        		<span style = "display:none">&nbsp;</span>
        </div>
    </div>
                            </td></tr>
        
                            </table>
                        </li>
    	    						<li id = "second_empty" style = "display:none;"></li>
					</ul>
                </div>
                <div style="width:50%; height:100%;">
                    <ul class="sortable" id="firstlist" style="width:100%;">
                                <li id="firstlist_moduleIconFunctions">
                            <table class = "singleColumnData">
                                    	<tr><td class = "moduleCell">
        	
    <div class = "block" style = ";" id = "Options" >
        <div class = "blockContents" >
    			<span class = "handles"><img  src = 'themes/default/images/others/transparent.gif'  class = 'open open-close-handle sprite16 sprite16-navigate_up' alt = "Expand/collapse block" title = "Expand/collapse block" onclick = "toggleBlock(this, '7c439645511a8cec23b90b6fc07c1377')"  id = "Options_image"><img  src = 'themes/default/images/others/transparent.gif'  class = 'blockMoveHandle sprite16 sprite16-attachment' alt = "Move block" title = "Move block" onmousedown = "createSortable('firstlist');createSortable('secondlist');if (window.showBorders) showBorders(event)" onmouseup = "if (window.showBorders) hideBorders(event)"></span>
        		<span class = "title">Options</span>
        		<span class = "subtitle"></span>
        		
        		<div class = "content" style = ";" id = "Options_content" onmousedown = "if ($('firstlist')) {Sortable.destroy('firstlist');}if ($('secondlist')) {Sortable.destroy('secondlist');}">
					
	    		<table class = "iconTable"><tr>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=users'">
                        	<a  href = "administrator.php?ctg=users" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-user' title = "Users" alt = "Users" /><br>
                        		Users
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=lessons'">
                        	<a  href = "administrator.php?ctg=lessons" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-lessons' title = "Lessons" alt = "Lessons" /><br>
                        		Lessons
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=courses'">
                        	<a  href = "administrator.php?ctg=courses" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-courses' title = "Courses" alt = "Courses" /><br>
                        		Courses
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=directions'">
                        	<a  href = "administrator.php?ctg=directions" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-categories' title = "Categories" alt = "Categories" /><br>
                        		Categories
                        	</a>
                        </td></tr><tr>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=user_types'">
                        	<a  href = "administrator.php?ctg=user_types" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-user_types' title = "User types" alt = "User types" /><br>
                        		User types
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=user_groups'">
                        	<a  href = "administrator.php?ctg=user_groups" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-users' title = "Groups" alt = "Groups" /><br>
                        		Groups
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=system_config'">
                        	<a  href = "administrator.php?ctg=system_config" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-tools' title = "System settings" alt = "System settings" /><br>
                        		System settings
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=themes&theme=2'">
                        	<a  href = "administrator.php?ctg=themes&theme=2" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-themes' title = "Themes" alt = "Themes" /><br>
                        		Themes
                        	</a>
                        </td></tr><tr>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=digests'">
                        	<a  href = "administrator.php?ctg=digests" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-notifications' title = "Notifications" alt = "Notifications" /><br>
                        		Notifications
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=messages'">
                        	<a  href = "administrator.php?ctg=messages" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-mail' title = " Messages" alt = " Messages" /><br>
                        		 Messages
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=logout_user'">
                        	<a  href = "administrator.php?ctg=logout_user" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-logout' title = "Connected users" alt = "Connected users" /><br>
                        		Connected users
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=import_export'">
                        	<a  href = "administrator.php?ctg=import_export" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-import_export' title = "Export - Import" alt = "Export - Import" /><br>
                        		Export - Import
                        	</a>
                        </td></tr><tr>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=languages'">
                        	<a  href = "administrator.php?ctg=languages" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-languages' title = "Languages" alt = "Languages" /><br>
                        		Languages
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=statistics'">
                        	<a  href = "administrator.php?ctg=statistics" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-reports' title = "Reports" alt = "Reports" /><br>
                        		Reports
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=backup'">
                        	<a  href = "administrator.php?ctg=backup" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-backup_restore' title = "Backup - Restore" alt = "Backup - Restore" /><br>
                        		Backup - Restore
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=maintenance'">
                        	<a  href = "administrator.php?ctg=maintenance" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-maintenance' title = "Maintenance" alt = "Maintenance" /><br>
                        		Maintenance
                        	</a>
                        </td></tr><tr>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=forum'">
                        	<a  href = "administrator.php?ctg=forum" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-forum' title = "Forum" alt = "Forum" /><br>
                        		Forum
                        	</a>
                        </td><td></td><td></td><td></td></table><fieldset class = "fieldsetSeparator"><legend>Modules</legend>
	    		<table class = "iconTable"><tr>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=modules'">
                        	<a  href = "administrator.php?ctg=modules" >
                        		<img  src = 'themes/default/images/others/transparent.gif' class = 'sprite32 sprite32-addons' title = "Modules" alt = "Modules" /><br>
                        		Modules
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=module&op=module_bbb'">
                        	<a  href = "administrator.php?ctg=module&op=module_bbb" >
                        		<img  src = 'http://localhost:8080/efront/www/modules/module_bbb/images/bbb32.png' title = "BBB Conferences" alt = "BBB Conferences" /><br>
                        		BBB Conferences
                        	</a>
                        </td>
                    	<td style = 'width:25%;' class = 'iconData' onclick = ";location='administrator.php?ctg=module&op=module_security'">
                        	<a  href = "administrator.php?ctg=module&op=module_security" >
                        		<img  src = 'http://localhost:8080/efront/www/modules/module_security/img/security_agent.png' title = "Security module" alt = "Security module" /><br>
                        		Security module
                        	</a>
                        </td><td></td></table></fieldset>
				</div>
        		<span style = "display:none">&nbsp;</span>
        </div>
    </div>
        </td></tr>
        
                            </table>
                        </li>
    	    
                        <li id = "first_empty" style = "display:none;"></li>
                    </ul>
                </div>
			</div>
       </td>
   </tr>


		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
  		 
	</table>

				</td>
				<td class = "layoutColumn right">
									</td></tr>
			</table>
		</td></tr>
			<tr><td style = "vertical-align:bottom">
			<table style = "width:100%">
				<tr><td class = "footer " colspan = "3">		
		<div><a href = "http://www.efrontlearning.net">eFront</a> (version 3.6.15) &bull; Community Edition &bull; <a href = "index.php?ctg=contact">Contact us</a></div>
	</td></tr>
			</table>
		</td></tr>
		</table>





<script>
var currentUnit = '';		var g_servername = 'http://localhost:8080/efront/www/';
</script>
<script>var BOOKMARKTRANSLATION = 'Bookmarks';var NODATAFOUND = '-';</script>

<script type = "text/javascript" src = "js/scripts.php?build=18025&load=scriptaculous/prototype,scriptaculous/scriptaculous,scriptaculous/effects,prototip/prototip,efront_ajax,EfrontScripts,includes/events"> </script>	


<script type = "text/javascript" src = "js/scripts.php?build=18025&load=scriptaculous/dragdrop,includes/control_panel"> </script>

<div id = "user_table" style = "display:none">

    <div class = "block" style = ";" id = "Information" >
        <div class = "blockContents" >
    			<span class = "handles"><img  src = 'themes/default/images/others/transparent.gif'  class = 'open open-close-handle sprite16 sprite16-navigate_up' alt = "Expand/collapse block" title = "Expand/collapse block" onclick = "toggleBlock(this, 'eaba1384ad2f2246ad21ce559e77c49a')"  id = "Information_image"><img  src = 'themes/default/images/others/transparent.gif'  class = 'blockMoveHandle sprite16 sprite16-attachment' alt = "Move block" title = "Move block" onmousedown = "createSortable('firstlist');createSortable('secondlist');if (window.showBorders) showBorders(event)" onmouseup = "if (window.showBorders) hideBorders(event)"></span>
        		<span class = "title">Information</span>
        		<span class = "subtitle"></span>
        		
        		<div class = "content" style = ";" id = "Information_content" onmousedown = "if ($('firstlist')) {Sortable.destroy('firstlist');}if ($('secondlist')) {Sortable.destroy('secondlist');}">
					    <table width = "100%">
        <tr><td align = "left" id = "user_box" style = "padding:3px 3px 4px 5px;"></td></tr>
    </table>

				</div>
        		<span style = "display:none">&nbsp;</span>
        </div>
    </div>
</div>

<table id = "popup_table" class = "divPopup" style = "display:none;">
    <tr class = "defaultRowHeight">
        <td class = "topTitle" id = "popup_title"></td>
        <td class = "topTitle" id = "popup_close_cell"><img src = 'themes/default/images/others/transparent.gif' class = 'sprite16 sprite16-close' alt = "Close" name = "" id = "popup_close" title = "Close" onclick = "if (document.getElementById('reloadHidden') && document.getElementById('reloadHidden').value == '1')  {parent.frames[1].location = parent.frames[1].location};eF_js_showDivPopup();"/>
    </td></tr>
    <tr><td colspan = "2" id = "popup_data" style = ""></td></tr>
    <tr><td colspan = "2" id = "frame_data" style = "display:none;">
		<iframe  width="100%" height="600px" name = "POPUP_FRAME" id = "popup_frame" src = "javascript:''" >Sorry, but your browser needs to support iframes to see this</iframe>
    </td></tr>
</table>
<div id = "error_details" style = "display:none">
    <div class = "block" style = ";" id = "Error+Details" >
        <div class = "blockContents" >
    			<span class = "handles"><img  src = 'themes/default/images/others/transparent.gif'  class = 'open open-close-handle sprite16 sprite16-navigate_up' alt = "Expand/collapse block" title = "Expand/collapse block" onclick = "toggleBlock(this, '95878e9edf4285764c70751a465431dd')"  id = "Error+Details_image"><img  src = 'themes/default/images/others/transparent.gif'  class = 'blockMoveHandle sprite16 sprite16-attachment' alt = "Move block" title = "Move block" onmousedown = "createSortable('firstlist');createSortable('secondlist');if (window.showBorders) showBorders(event)" onmouseup = "if (window.showBorders) hideBorders(event)"></span>
        		<span class = "title">Error Details</span>
        		<span class = "subtitle"></span>
        		
        		<div class = "content" style = ";" id = "Error+Details_content" onmousedown = "if ($('firstlist')) {Sortable.destroy('firstlist');}if ($('secondlist')) {Sortable.destroy('secondlist');}">
					<pre></pre>
				</div>
        		<span style = "display:none">&nbsp;</span>
        </div>
    </div></div>
<div id = 'showMessageDiv' style = "display:none"></div>
<div id="dimmer" class = "dimmerDiv" style = "display:none;"></div>
<div id = "defaultExceptionHandlerDiv" style = "color:#ffffff;display:none"></div>



<script>




    var __shouldTriggerNextNotifications = false;




if (!usingHorizontalInterface) {
	if (top.sideframe && top.sideframe.document.getElementById('current_location')) {
		top.sideframe.document.getElementById('current_location').value = top.mainframe.location.toString();
	}
} else {
	// $('current_location') caused js error in browse.php
	if (document.getElementById('current_location')) {
		document.getElementById('current_location').value = document.location.toString();
	}
}


	translations['_COUPON'] = 'Coupon';
	translations['_CLICKTOENTERDISCOUNTCOUPON'] = 'Click to enter discount coupon';
						redirectLocation ='/efront/www/administrator.php?ctg=lessons&catalog=1&checkout=1';
			
			if (parent.frames[0].document.getElementById('dimmer'))
		parent.frames[0].document.getElementById('dimmer').style.display = 'none';

		if (top.sideframe && top.sideframe.document && top.sideframe.document.getElementById('loading_sidebar'))
	    top.sideframe.document.getElementById('loading_sidebar').style.display = 'none';        //no prototype here please
			
</script>

<script>

if (window.location.href.indexOf("#_=_") > 0) { window.location = window.location.href.replace(/#.*/, ""); }

</script>

		
 

</body>
</html>


<script>if (__shouldTriggerNextNotifications) { new Ajax.Request("send_notifications.php?ajax=1", {method:'get', asynchronous:true}); } </script><script type = "text/javascript" src = "js/scripts.php?build=18025&load=drag-drop-folder-tree"> </script>