


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html    >
<head>
   <!-- <base href = "http://localhost:8080/efront/www/">    -->
    <meta http-equiv = "Content-Language" content = "en">
    <meta http-equiv = "keywords"         content = "education">
    <meta http-equiv = "description"      content = "Collaborative Elearning Platform">
    <meta http-equiv = "Content-Type"     content = "text/html; charset = utf-8">
    <!--  <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>  -->
    <img src ="logo.png">
    <link rel = "stylesheet" type = "text/css" href = "efrontdemo\css_global.css">
        <title>eFront | Refreshing eLearning</title>
<!-- 
    -->
    <link rel = "stylesheet" type = "text/css" href = "efrontdemo\bootstrap\css\bootstrap.css" />
    <link rel = "stylesheet" type = "text/css" href = "efrontdemo\includes\imgareaselect\css\imgareaselect-default.css" />
    <script src = "efrontdemo\bootstrap\jquery.js"></script>
    <script src = "efrontdemo\bootstrap\js\bootstrap.js"></script>
    <script src = "efrontdemo\js\scripts.js"></script>
    <script src = "efrontdemo\includes\imgareaselect\scripts\jquery.imgareaselect.pack.js"></script>
    <script type = "text/javascript">
        var ajaxObjects    = new Array();
                                
        top.document.title = "eFront | Refreshing eLearning";
        if (window.name == 'POPUP_FRAME') var popup=1;
        var globalImageExtension = 'png';
                var usingHorizontalInterface = true;
        
        var sessionLogin = "admin";
        var translationsToJS = new Array();
    </script>


<script>var translations = new Array(); /*used for passing language tags to js*/</script>

</head>
<body id = "body_control_panel" onkeypress = "if (window.eF_js_keypress) eF_js_keypress(event);" onbeforeunload = "if (window.periodicUpdater) periodicUpdater(false);">




<script language = "JavaScript" type = "text/javascript">



 </script>



        <script>
        // Translations used in the updater script
        
        translations['lessons']         = 'Lessons';
        translations['servername']      = 'http://localhost:8080/efront/www/';
        translations['onlineusers']     = 'Connected';
        translations['nousersinroom']   = 'No user currently in this chat room';
        translations['redirectedtomain']= 'Redirected to general room';
        translations['s_type']          = 'administrator';
        translations['s_login']         = 'admin';
        translations['clicktochange']   = 'Click to change status';
        translations['userisonline']    = 'The user is online';
        translations['and']             = 'and';
        translations['hours']           = 'hours';
        translations['minutes']         = 'minutes';
        translations['userjustloggedin']= 'The user just logged in';
        translations['user']            = 'User';
        translations['sendmessage']     = 'Send message';
        translations['web']             = 'Personal page';
        translations['user_stats']      = 'User reports';
        translations['user_settings']   = 'Public profile';
        translations['logout_user']     = 'Log out user';
        translations['_ADMINISTRATOR']  = 'Administrator';
        translations['_PROFESSOR']      = 'Professor';
        translations['_STUDENT']        = 'Student';
        translations['_IRREVERSIBLEACTIONAREYOUSURE'] = 'This operation is irreversible! Are you sure?';
    
        var startUpdater = true;
        var updaterPeriod = '100000';
    </script>
        <table class = "pageLayout centerFull" id = "pageLayout" style="margin-top: 5px;">
        <tr><td style = "vertical-align:top">
            <table style = "width:100%;">
                            <tr><td id ="horizontalBarRow" class = "header" colspan = "3">  <div id = "logo">
      
    </div>
    
        <div id = "logout_link" >
                                                        <span class="headerText dropdown">
                        <span class = "label" style = "cursor:pointer" title = "Switch to simple mode" onclick = "jQuery.fn.efront('switchmode')">Complete mode</span>
                    </span>
                <span class="headerText dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="efront/www/userpage.php">Go to <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="efront/www/userpage.php">Home</a></li>
                        <li><a href="efront/www/userpage.php?ctg=users">Users</a></li>
                        <li><a href="efront/www/userpage.php?ctg=courses">Courses</a></li>
                        <li><a href="efront/www/userpage.php?ctg=lessons">Lessons</a></li>
                        <li class="divider"></li>
                        <li class="nav-header">Add</li>
                        <li><a href="efront/www/userpage.php?ctg=personal&user=admin&op=profile&add_user=1">User </a></li>
                        <li><a href="efront/www/userpage.php?ctg=courses&add_course=1">Course</a></li>
                        <li><a href="efront/www/userpage.php?ctg=lessons&add_lesson=1">Lesson</a></li>
                    </ul>
                </span>
                <span class="headerText dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">Administrator S. (admin) <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="nav-header">Switch account</li>
                        <li><a href="javascript:void(0)" onclick = "changeAccount('professor')">Professor D. (professor)</a></li>
                        <li><a href="javascript:void(0)" onclick = "changeAccount('student')">Student D. (student)</a></li>
                        <li class="divider"></li>
                        <li><a href="efront/www/userpage.php?ctg=personal&user=admin&op=dashboard">Dashboard</a></li>
                        <li><a href="efront/www/userpage.php?ctg=personal&user=admin&op=profile">Account</a></li>
                    </ul>
                </span>             
                            
                <span class="headerText dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Messages <b class="caret"></b></a>     <ul class="dropdown-menu">
                    
                        <li><a href="LMSDemo(efront)/efront/www/userpage.php?ctg=messages">Incoming</a></li>
                        <li><a href="LMSDemo(efront)/efront/www/userpage.php?ctg=messages&add=1">Create</a></li>
                    </ul>
                                    
                </span>
                                        
                <span class = "headerText">
                <form action = "efront/www/administrator.php?ctg=control_panel&op=search" method = "post" style = "display:inline-block;">
                <input type = "text" name = "search_text" placeholder = "Search" class = "searchBox"/>
                <input type = "hidden" name = "current_location" id = "current_location" />
                </form>
                </span>
                                        
                <a class = "headerText" href = "efront/www/index.php?logout=true">Logout</a>
        </div>
            
        <div id = "path">
        
        <div id = "path_title"><a class = "titleLink" title="Home" href ="efront/www/administrator.php?ctg=control_panel">Home</a></div>
        
        <div id = "tab_handles_div"></div>
    </div>
</td>
</tr>
<tr><td class = "layoutColumn left"></td>
    <td class = "layoutColumn center">
    <table class = "centerTable">
        
<?php
include_once 'dbconfig2.php';

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>File Uploading</title>
<!-- <link rel="stylesheet" href="style.css" type="text/css" /> -->
</head>
<body>

<div id="body">
 <form action="upload1.php" method="post" enctype="multipart/form-data" height="50px" width="100px">
 <input type="file" name="file" />
 <button type="submit" name="btn-upload">Upload</button>
 </form>
    <br /><br />
    
</div>

</body>
</html>
