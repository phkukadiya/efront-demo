<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html    >
<head>
   <!-- <base href = "http://localhost:8080/efront/www/">    -->
    <meta http-equiv = "Content-Language" content = "en">
    <meta http-equiv = "keywords"         content = "education">
    <meta http-equiv = "description"      content = "Collaborative Elearning Platform">
    <meta http-equiv = "Content-Type"     content = "text/html; charset = utf-8">
    <!--  <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>  -->
    <img src ="logo.png">
    <link rel = "stylesheet" type = "text/css" href = "efrontdemo\css_global.css">
        <title>eFront | Refreshing eLearning</title>
<!-- 
    -->
    <link rel = "stylesheet" type = "text/css" href = "efrontdemo\bootstrap\css\bootstrap.css" />
    <link rel = "stylesheet" type = "text/css" href = "efrontdemo\includes\imgareaselect\css\imgareaselect-default.css" />
    <script src = "efrontdemo\bootstrap\jquery.js"></script>
    <script src = "efrontdemo\bootstrap\js\bootstrap.js"></script>
    <script src = "efrontdemo\js\scripts.js"></script>
    <script src = "efrontdemo\includes\imgareaselect\scripts\jquery.imgareaselect.pack.js"></script>
    <script type = "text/javascript">
        var ajaxObjects    = new Array();
                                
        top.document.title = "eFront | Refreshing eLearning";
        if (window.name == 'POPUP_FRAME') var popup=1;
        var globalImageExtension = 'png';
                var usingHorizontalInterface = true;
        
        var sessionLogin = "admin";
        var translationsToJS = new Array();
    </script>


<script>var translations = new Array(); /*used for passing language tags to js*/</script>

</head>
<body id = "body_control_panel" onkeypress = "if (window.eF_js_keypress) eF_js_keypress(event);" onbeforeunload = "if (window.periodicUpdater) periodicUpdater(false);">




<script language = "JavaScript" type = "text/javascript">



 </script>



        <script>
        // Translations used in the updater script
        
        translations['lessons']         = 'Lessons';
        translations['servername']      = 'http://localhost:8080/efront/www/';
        translations['onlineusers']     = 'Connected';
        translations['nousersinroom']   = 'No user currently in this chat room';
        translations['redirectedtomain']= 'Redirected to general room';
        translations['s_type']          = 'administrator';
        translations['s_login']         = 'admin';
        translations['clicktochange']   = 'Click to change status';
        translations['userisonline']    = 'The user is online';
        translations['and']             = 'and';
        translations['hours']           = 'hours';
        translations['minutes']         = 'minutes';
        translations['userjustloggedin']= 'The user just logged in';
        translations['user']            = 'User';
        translations['sendmessage']     = 'Send message';
        translations['web']             = 'Personal page';
        translations['user_stats']      = 'User reports';
        translations['user_settings']   = 'Public profile';
        translations['logout_user']     = 'Log out user';
        translations['_ADMINISTRATOR']  = 'Administrator';
        translations['_PROFESSOR']      = 'Professor';
        translations['_STUDENT']        = 'Student';
        translations['_IRREVERSIBLEACTIONAREYOUSURE'] = 'This operation is irreversible! Are you sure?';
    
        var startUpdater = true;
        var updaterPeriod = '100000';
    </script>
        <table class = "pageLayout centerFull" id = "pageLayout" style="margin-top: 5px;">
        <tr><td style = "vertical-align:top">
            <table style = "width:100%;">
                            <tr><td id ="horizontalBarRow" class = "header" colspan = "3">  <div id = "logo">
      
    </div>
    
        <div id = "logout_link" >
                                                        <span class="headerText dropdown">
                        <span class = "label" style = "cursor:pointer" title = "Switch to simple mode" onclick = "jQuery.fn.efront('switchmode')">Complete mode</span>
                    </span>
                                                <span class="headerText dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Go to <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="efront/www/userpage.php">Home</a></li>
                                                <li><a href="userpage.php?ctg=users">Users</a></li>
                                                                        <li><a href="efront/www/userpage.php?ctg=courses">Courses</a></li>
                        <li><a href="efront/www/userpage.php?ctg=lessons">Lessons</a></li>
                                
                                            <li class="divider"></li>
                        <li class="nav-header">Add</li>
                                                
                        <li><a href="efront/www/userpage.php?ctg=personal&user=admin&op=profile&add_user=1">User</a></li>
                                                                        <li><a href="efront/www/userpage.php?ctg=courses&add_course=1">Course</a></li>
                        <li><a href="efront/www/userpage.php?ctg=lessons&add_lesson=1">Lesson</a></li>
                                
                                        </ul>
                </span>
                                <span class="headerText dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">Administrator S. (admin) <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                                            <li class="nav-header">Switch account</li>
                                                                            <li><a href="efront/www/professor.php?ctg=control_panel" onclick = "changeAccount('professor')">Professor D. (professor)</a></li>
                                                    <li><a href="efront/www/student.php" onclick = "changeAccount('student')">Student D. (student)</a></li>
                                                                            <li class="divider"></li>
                                                                <li><a href="efront/www/userpage.php?ctg=personal&user=admin&op=dashboard">Dashboard</a></li>
                                            <li><a href="efront/www/userpage.php?ctg=personal&user=admin&op=profile">Account</a></li>
                                                            </ul>
                </span>             
                            <span class="headerText dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Messages <b class="caret"></b></a>                                      <ul class="dropdown-menu">
                        <li><a href="efront/www/userpage.php?ctg=messages">Incoming</a></li>
                        <li><a href="efront/www/userpage.php?ctg=messages&add=1">Create</a></li>
                    </ul>
                                    </span>
                                        <span class = "headerText">
                <form action = "efront/www/administrator.php?ctg=control_panel&op=search" method = "post" style = "display:inline-block;">
                    <input type = "text" name = "search_text" placeholder = "Search" class = "searchBox"/>
                    <input type = "hidden" name = "current_location" id = "current_location" />
                </form>
                </span>
                                        
                            <a class = "headerText" href = "efront/www/index.php?logout=true">Logout</a>
                                </div>
            <div id = "path">
        <div id = "path_title"><a class = "titleLink" title="Home" href ="efront/www/administrator.php?ctg=control_panel">Home</a></div>
        <div id = "tab_handles_div">
                    </div>
    </div>
</td></tr>
                        <tr><td class = "layoutColumn left">
                                    </td>
                <td class = "layoutColumn center">
                                    <table class = "centerTable">
        
          
<?php
    include 'connection.php';

    $sql = "SELECT `id`, `title`, `views`, `type` FROM uploads";
    $result = $connection->query($sql);

    $pagetitle = "All Uploads";
    include 'header.php';
    /*echo ini_get('post_max_size');
    echo ini_get('memory_limit');*/
?>

<div id="content">

    <h1>Upload Content</h1>

    <form action="upload.php" method="POST" enctype="multipart/form-data">
        <input type="file" name="uploadFile" id="uploadFile" value ="upload video"><br />
        <input type="text" id="title" name="title" placeholder="Title" /><br />
        <textarea id="description" name="description" placeholder="Description"rows="3"></textarea><br />
        <input type="submit" value="Upload video" name="submit"> 
       </form>
  

   <!--  <h1>Analytics</h1>

    <?php include('uploads-table.php'); ?> -->

</div>

<?php include 'footer.php'; ?>
        
                            </table>
                        </li>
            
                        <li id = "first_empty" style = "display:none;"></li>
                    </ul>
                </div>
            </div>
       </td>
   </tr>


         </table>

                </td>
                <td class = "layoutColumn right">
                                    </td></tr>
            </table>
        </td></tr>
            <tr><td style = "vertical-align:bottom">
           



<script>
var currentUnit = '';       var g_servername = 'http://localhost:8080/efront/www/';
</script>
<script>var BOOKMARKTRANSLATION = 'Bookmarks';var NODATAFOUND = '-';</script>

<script type = "text/javascript" src = "js/scripts.php?build=18025&load=scriptaculous/prototype,scriptaculous/scriptaculous,scriptaculous/effects,prototip/prototip,efront_ajax,EfrontScripts,includes/events"> </script>   


<script type = "text/javascript" src = "js/scripts.php?build=18025&load=scriptaculous/dragdrop,includes/control_panel"> </script>

<div id = "user_table" style = "display:none">

    <div class = "block" style = ";" id = "Information" >
        <div class = "blockContents" >
                <span class = "handles"><img  src = 'themes/default/images/others/transparent.gif'  class = 'open open-close-handle sprite16 sprite16-navigate_up' alt = "Expand/collapse block" title = "Expand/collapse block" onclick = "toggleBlock(this, 'eaba1384ad2f2246ad21ce559e77c49a')"  id = "Information_image"><img  src = 'themes/default/images/others/transparent.gif'  class = 'blockMoveHandle sprite16 sprite16-attachment' alt = "Move block" title = "Move block" onmousedown = "createSortable('firstlist');createSortable('secondlist');if (window.showBorders) showBorders(event)" onmouseup = "if (window.showBorders) hideBorders(event)"></span>
                <span class = "title">Information</span>
                <span class = "subtitle"></span>
                
                <div class = "content" style = ";" id = "Information_content" onmousedown = "if ($('firstlist')) {Sortable.destroy('firstlist');}if ($('secondlist')) {Sortable.destroy('secondlist');}">
                        <table width = "100%">
        <tr><td align = "left" id = "user_box" style = "padding:3px 3px 4px 5px;"></td></tr>
    </table>

                </div>
                <span style = "display:none">&nbsp;</span>
        </div>
    </div>
</div>

<table id = "popup_table" class = "divPopup" style = "display:none;">
    <tr class = "defaultRowHeight">
        <td class = "topTitle" id = "popup_title"></td>
        <td class = "topTitle" id = "popup_close_cell"><img src = 'themes/default/images/others/transparent.gif' class = 'sprite16 sprite16-close' alt = "Close" name = "" id = "popup_close" title = "Close" onclick = "if (document.getElementById('reloadHidden') && document.getElementById('reloadHidden').value == '1')  {parent.frames[1].location = parent.frames[1].location};eF_js_showDivPopup();"/>
    </td></tr>
    <tr><td colspan = "2" id = "popup_data" style = ""></td></tr>
    <tr><td colspan = "2" id = "frame_data" style = "display:none;">
        <iframe  width="100%" height="600px" name = "POPUP_FRAME" id = "popup_frame" src = "javascript:''" >Sorry, but your browser needs to support iframes to see this</iframe>
    </td></tr>
</table>
<div id = "error_details" style = "display:none">
    <div class = "block" style = ";" id = "Error+Details" >
        <div class = "blockContents" >
                <span class = "handles"><img  src = 'themes/default/images/others/transparent.gif'  class = 'open open-close-handle sprite16 sprite16-navigate_up' alt = "Expand/collapse block" title = "Expand/collapse block" onclick = "toggleBlock(this, '95878e9edf4285764c70751a465431dd')"  id = "Error+Details_image"><img  src = 'themes/default/images/others/transparent.gif'  class = 'blockMoveHandle sprite16 sprite16-attachment' alt = "Move block" title = "Move block" onmousedown = "createSortable('firstlist');createSortable('secondlist');if (window.showBorders) showBorders(event)" onmouseup = "if (window.showBorders) hideBorders(event)"></span>
                <span class = "title">Error Details</span>
                <span class = "subtitle"></span>
                
                <div class = "content" style = ";" id = "Error+Details_content" onmousedown = "if ($('firstlist')) {Sortable.destroy('firstlist');}if ($('secondlist')) {Sortable.destroy('secondlist');}">
                    <pre></pre>
                </div>
                <span style = "display:none">&nbsp;</span>
        </div>
    </div></div>
<div id = 'showMessageDiv' style = "display:none"></div>
<div id="dimmer" class = "dimmerDiv" style = "display:none;"></div>
<div id = "defaultExceptionHandlerDiv" style = "color:#ffffff;display:none"></div>



<script>




    var __shouldTriggerNextNotifications = false;




if (!usingHorizontalInterface) {
    if (top.sideframe && top.sideframe.document.getElementById('current_location')) {
        top.sideframe.document.getElementById('current_location').value = top.mainframe.location.toString();
    }
} else {
    // $('current_location') caused js error in browse.php
    if (document.getElementById('current_location')) {
        document.getElementById('current_location').value = document.location.toString();
    }
}


    translations['_COUPON'] = 'Coupon';
    translations['_CLICKTOENTERDISCOUNTCOUPON'] = 'Click to enter discount coupon';
                        redirectLocation ='/efront/www/administrator.php?ctg=lessons&catalog=1&checkout=1';
            
            if (parent.frames[0].document.getElementById('dimmer'))
        parent.frames[0].document.getElementById('dimmer').style.display = 'none';

        if (top.sideframe && top.sideframe.document && top.sideframe.document.getElementById('loading_sidebar'))
        top.sideframe.document.getElementById('loading_sidebar').style.display = 'none';        //no prototype here please
            
</script>

<script>

if (window.location.href.indexOf("#_=_") > 0) { window.location = window.location.href.replace(/#.*/, ""); }

</script>

        
 

</body>
</html>


<script>if (__shouldTriggerNextNotifications) { new Ajax.Request("send_notifications.php?ajax=1", {method:'get', asynchronous:true}); } </script><script type = "text/javascript" src = "js/scripts.php?build=18025&load=drag-drop-folder-tree"> </script>

 <table style = "width:100%">
                <tr><td class = "footer " colspan = "3">        
        <div><a href = "http://www.efrontlearning.net">eFront</a> (version 3.6.15) &bull; Community Edition &bull; <a href = "index.php?ctg=contact">Contact us</a></div>
    </td></tr>
            </table>
        </td></tr>
        </table>

